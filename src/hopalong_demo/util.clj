(ns hopalong-demo.util
  (:require [clojure.algo.generic.math-functions :refer [sgn]]
	    [clojure.math.numeric-tower :refer [expt abs]]))

(defn random-color []
  {:r (rand-int 256)
   :g (rand-int 256)
   :b (rand-int 256)})

(defn random-point []
  {:x (rand 10)
   :y (rand 10)})

(defn hopalong [a b c {:keys [x y]}]
  {:x (- y (* (sgn x) (expt (abs (- (* b x) c)) 0.5)))
   :y (- a x)})
