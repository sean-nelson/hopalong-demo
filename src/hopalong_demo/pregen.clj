(ns hopalong-demo.pregen
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [hopalong-demo.util :as u]))

(def resolution [1366 768])
(def scale 50)
(def a 0.5)
(def b -0.6)
(def c 0)
(def iterations 2000)

(def hopalong (partial u/hopalong a b c))

(defn setup []
  ; setup function returns initial state.
  (q/background 0)
  (q/frame-rate 30)
  (repeatedly 50 #(take iterations (iterate hopalong (u/random-point)))))

(defn draw-state [state]
  ; Set circle color.
  (q/stroke-weight 2)
  (let [[width height] resolution
        xt             (/ width 2)
        yt             (/ height 2)]
    (doseq [orbit state]
      (let [{:keys [r g b]} (u/random-color)]
        (q/stroke r g b)
        (doseq [{:keys [x y]} orbit]
          (q/with-translation [xt yt]
            (q/point (* scale x) (* scale y))))))))


(q/defsketch hopalong-layer
  :title "Hopalong Layer"
  :size resolution
  ; setup function called only once, during sketch initialization.
  :setup setup
  :draw draw-state
  :features [:keep-on-top]
  :middleware [m/fun-mode])
