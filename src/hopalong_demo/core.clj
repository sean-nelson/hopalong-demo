(ns hopalong-demo.core
  (:require [quil.core :as q]
            [quil.middleware :as m]
            [hopalong-demo.util :as u]))

(def resolution [1920 1080])
(def a 0.5)
(def b -0.6)
(def c 0)

(def hopalong (partial u/hopalong a b c))

(defn bodies []
  (lazy-seq (cons {:point (u/random-point)
                   :color (u/random-color)} (bodies))))

(defn setup []
  ; Set frame rate to 60 frames per second.
  (q/frame-rate 60)
  ; setup function returns initial state.
  (q/background 0)
  (q/blend-mode :blend)
  (take 1000 (bodies)))
  
  
(defn update-body [{:keys [point color]}]
  {:point (hopalong point)
   :color color})
  
(defn update-state [state]
  (map update-body state))

(defn draw-state [state]
  ; Set circle color.
  (q/stroke-weight 2)
  (let [[width height] resolution
        xt             (/ width 2)
        yt             (/ height 2)
        scale          50]
    (doseq [{{:keys [x y]}   :point 
             {:keys [r g b]} :color} state]
       (q/stroke r g b)
       (q/with-translation [xt yt]
            (q/point (* scale x) (* scale y))))))


(q/defsketch hopalong-demo
  :title "Hopalong Demo"
  :size resolution
  ; setup function called only once, during sketch initialization.
  :setup setup
  ; update-state is called on each iteration before draw-state.
  :update update-state
  :draw draw-state
  :features [:keep-on-top]
  :middleware [m/fun-mode])
